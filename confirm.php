<?php

/*
 * this is only called in a server-server communication
 */
header('content-type: text/json');

if(
	empty($_GET['api_key'])
	|| $_GET['api_key'] != '12345'
){
	header("HTTP/1.0 403 Forbidden");
	echo json_encode(array('error'=>'invalid api key'));
	die();
}

$secret = '12345';

list($hash, $unique_id) = explode('.', $_GET['unique_id']);

if(
	$hash == sha1($unique_id.'-'.$secret)
){
	header("HTTP/1.0 200");
	echo json_encode(
		array(
			'success'		=> true,
			'first_name'	=> 'Max',
			'last_name'		=> 'Mustermann',
			'email'			=> 'none@local.at',
			'username'		=> 'max.mustermann',
			'id'			=> '1234'
		)
	);
	
	die();
}

header("HTTP/1.0 404 Not Found");
echo json_encode(array('error'=>'unique id was already consumed'));
die();
