# Example SSO Implementation #

how to handle it with ladenseite (**warning:** might change in the future!!)


## Installation ##

Checkout this repository at any given location.

```
Note:
the document root / domain must be reachable form the outside. 
file:// or .local are not working
```



### Credentials ###

```
Username:   my-test-account
Password:   my-test-password

First Name: Max
Last Name:  Mustermann
Email:      none@local.at
username:   max.mustermann
id:         1234
```

## Usage ##

In your Ladenseite.com Marketplace add authentication Method SSO and provide there the following settings:

```
Initiate Url: http://where.ever-it-is.at/login.php?redirect=
Callback Url: http://where.ever-it-is.at/confirm.php?api_key=12345&unique_id={ID}
Type:         default
```
![configuration_example_marketplace.png](https://bitbucket.org/repo/EpzX9K/images/2680172783-configuration_example_marketplace.png)

Next open your marketplace and goto login, there select your new authentication method.

## Technical Details ##

You will be redirected to your domain with some url parameters.

Once the authentication was successful redirect to the response url and append an identifier.
ie: https://mymarketplace.testing.ladenseite.com/login/auth/1233sdffsd/confirm?id=12345

once this page is called it will fetch additional information from callback url with your id.
http://sso.dev.ms07.at/confirm.php?api_key=12345&unique_id=b6443655fcdb9871f0b98ed03b0e9bd5ad88eddd.1414662787
Response should contain the following Values (Json Encoded)

 - first_name
 - last_name
 - email
 - username
 - id (this is a unique identifier for your system)