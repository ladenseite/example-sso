<?php

$secret = '12345';

$forward_to = $_GET['redirect'];

if(!empty($_POST)){
	
	if(
		$_POST['user'] == 'my-test-account'
		&& $_POST['password'] == 'my-test-password'
	){
		$unique_id = time();
		
		$id = sha1($unique_id.'-'.$secret).'.'.$unique_id;
		
		/*
		 * login is done -> forward with a temporary id to to original system
		 *
		 * for security reasons the forward path should be restricted to specific domains (for us this is *.ladenseite.com)
		 */
		header('Location: '.$forward_to.'?id='.$id);
		echo 'redirect to '.$forward_to.'?id='.$id;
		die();
	}
	
}

echo <<<HTML
<h1>Please Login</h1>

<form method="post">
<input type="text" name="user">
<input type="password" name="password">
<input type="submit">
</form>
HTML;

